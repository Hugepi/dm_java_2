import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Arrays;
import java.util.*;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.isEmpty())
            return null;
        int min = liste.get(0);
        for(int nbr:liste){
            if(nbr < min)
                min = nbr;
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T elem:liste){
            if(valeur.compareTo(elem)>-1){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        ArrayList listeInter = new ArrayList();
        for(T elem:liste1){
            if(liste2.contains(elem) && !listeInter.contains(elem))
                listeInter.add(elem);
        }
            return listeInter;
    }
    





    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        ArrayList<String> liste = new ArrayList<String>();
        if(texte.length()!=0){
            for(String elem:texte.split(" ")){
                if(elem.length()!=0){
                    liste.add(elem);
                }
            }
        }
        return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        
        if(texte.length()==0)
            return null;

        List<String> phrase = decoupe(texte);
        Collections.sort(phrase);
        List<String> mots = new ArrayList<String>();
        List<Integer> nombreMots = new ArrayList<Integer>();
        

        for(String mot:phrase){
            if(!mots.contains(mot)){
                mots.add(mot);
                nombreMots.add(1);
            }else{
                nombreMots.set(mots.indexOf(mot), nombreMots.get(mots.indexOf(mot)) + 1);
            }
        }

        Integer max = nombreMots.get(0);
        for(Integer nbr:nombreMots){
            if(nbr>max){
                max = nbr;
            }
        }

        return mots.get(nombreMots.indexOf(max));

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int nbr = 0;
        if(chaine.length()%2 == 1)
            return false;
        for(int i = 0;i<chaine.length();i++){
            if(nbr<0)
                return false;
            if(chaine.charAt(i)=='(')
                nbr+=1;
            if(chaine.charAt(i)==')')
                nbr-=1;
        }
          return true;
      }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        int crochet=0;
        int parenthese=0;
        int parentheseF=0;
        int parentheseO=0;
        int crochetF=0;
        int crochetO=0;
  
        boolean res=true;
        char prochainfaux='a';
        for (int i = 0;i<chaine.length();i++){
  
            if (crochet<0 || parenthese<0)
                res=false;
  
            if (chaine.charAt(i)==prochainfaux)
                res=false;
  
            if(chaine.charAt(i)=='['){
                crochet+=1;
                prochainfaux=')';
                crochetO+=1;
            }
            if(chaine.charAt(i)=='('){
                parenthese+=1;
                prochainfaux=']';
                parentheseO+=1;
            }
  
            if(chaine.charAt(i)==')'){
                parenthese-=1;
                prochainfaux='a';
                parentheseF+=1;
            }

            if(chaine.charAt(i)==']'){
                crochet-=1;
                prochainfaux='a';
                crochetF+=1;
            }
        }
  
        if(parentheseO != parentheseF || crochetF != crochetO)
            res=false;
        if(parenthese%2 == 1 || crochet%2 == 1)
            res=false;
        return res;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
	    if(liste.isEmpty())
    	    return false;
        int sup = liste.size() - 1;
	    int inf = 0;
        int moy =  (inf + sup) / 2;
        while(inf <= sup) {
            if(liste.get(moy) == valeur)
                return true;
            else if(liste.get(moy) < valeur)
                inf = moy +1;
            else
                sup = moy - 1;
            moy = (inf + sup) / 2;
        }
        return false;
    }
}
